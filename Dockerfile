FROM registry.access.redhat.com/ubi8/ubi

ADD fio-2.2.8-43.LE.tar.gz /

# RUN echo "jill:200000:65536" >> /etc/subuid && echo "jill:200000:65536" >> /etc/subgid

# USER jill

EXPOSE 8765

CMD /usr/bin/fio --server
